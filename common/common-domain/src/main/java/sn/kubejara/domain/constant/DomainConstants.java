package sn.kubejara.domain.constant;


public class DomainConstants {

    private DomainConstants() {}

    public static final String UTC = "UTC";

    public static final int MIN_PARTANTS = 3;
    public static final int PREMIER_NUMERO = 1;

}
