package sn.kubejara.domain.constant;

import static sn.kubejara.domain.constant.DomainConstants.MIN_PARTANTS;

public class DomainMessages {
    private DomainMessages(){}


    public static final String NOMBRE_PARTANTS_INCORRECT_MESSAGE = String.format("Une course doit posséder au moins %d partants.", MIN_PARTANTS);
    public static final String DATE_COURSE_INCORRECTE_MESSAGE = "La date de la course est déjà passée.";

    public static final String NUMEROTATION_INCORRECTE_MESSAGE = "La numérotation est incorrecte.";
}
