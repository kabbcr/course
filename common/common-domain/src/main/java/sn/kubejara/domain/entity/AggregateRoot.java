package sn.kubejara.domain.entity;

/**
 * Cette classe est un marqueur. Elle permet de differencier une entité et un aggregateRoot
 * @param <ID>
 */
public abstract class AggregateRoot<ID> extends  BaseEntity<ID> {

}
