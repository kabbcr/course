package sn.kubejara.domain.event.publisher;


import sn.kubejara.domain.event.DomainEvent;

public interface DomainEventPublisher<T extends DomainEvent> {
    void publish(T domainEvent);
}
