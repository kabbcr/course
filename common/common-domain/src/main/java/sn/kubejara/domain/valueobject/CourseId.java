package sn.kubejara.domain.valueobject;

import java.util.UUID;

public class CourseId extends  BaseId<Long>{
    public CourseId(Long value) {
        super(value);
    }
}
