package sn.kubejara.domain.valueobject;

import java.util.UUID;

public class PartantId extends  BaseId<UUID>{
    public PartantId(UUID value) {
        super(value);
    }
}
