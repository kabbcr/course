package sn.kubejara.course.service.application.rest;

import lombok.extern.slf4j.Slf4j;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import sn.kubejara.dto.create.CourseDto;
import sn.kubejara.event.CourseCreatedEvent;
import sn.kubejara.ports.input.service.CourseApplicationService;

@Slf4j
@RestController
@RequestMapping(value = "/courses", produces = "application/vnd.api.v1+json")
public class CourseController {

    private final CourseApplicationService courseApplicationService;

    public CourseController(CourseApplicationService courseApplicationService) {
        this.courseApplicationService = courseApplicationService;
    }

    @PostMapping(value = "/creer")
    public ResponseEntity<CourseCreatedEvent> createAccount(@RequestBody CourseDto courseDto){
        log.info("Création de la course: {}", courseDto.getNumero());
        CourseCreatedEvent courseCreatedEvent = courseApplicationService.creerCourse(courseDto);
        log.info("Course numéro: {} créée avec succès", courseCreatedEvent.getCourse().getNumero());
        return  ResponseEntity.ok(courseCreatedEvent);
    }
}
