package sn.kubejara;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class BeanConfiguration {

    @Bean
    public CourseDomainService accountDomainService(){
        return new CourseDomainServiceImpl();
    }
}
