package sn.kubejara.adapter;

import org.springframework.stereotype.Component;
import sn.kubejara.entity.Course;
import sn.kubejara.mapper.CourseDataAccessMapper;
import sn.kubejara.ports.output.repository.CourseRepository;
import sn.kubejara.repository.CourseJpaRepository;

@Component
public class CourseRepositoryImpl implements CourseRepository {
    private final CourseJpaRepository courseJpaRepository;
    private final CourseDataAccessMapper courseDataAccessMapper;

    public CourseRepositoryImpl(CourseJpaRepository courseJpaRepository,
                                CourseDataAccessMapper courseDataAccessMapper) {
        this.courseJpaRepository = courseJpaRepository;
        this.courseDataAccessMapper = courseDataAccessMapper;
    }


    @Override
    public Course save(Course course) {
        return courseDataAccessMapper.courseEntityToCourse(courseJpaRepository.save(courseDataAccessMapper.courseToCourseEntity(course)));
    }
}
