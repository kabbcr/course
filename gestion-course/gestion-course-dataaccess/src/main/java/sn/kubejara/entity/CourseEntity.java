package sn.kubejara.entity;

import jakarta.persistence.*;
import lombok.*;

import java.time.ZonedDateTime;
import java.util.Set;

@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Table(name = "courses")
@Entity
public class CourseEntity {

    @Id
    private  Long numero;
    private  String nom;
    @Temporal(TemporalType.TIMESTAMP)
    private  ZonedDateTime date;
    @OneToMany(mappedBy = "course", cascade = CascadeType.ALL)
    private Set<PartantEntity> partants;

}
