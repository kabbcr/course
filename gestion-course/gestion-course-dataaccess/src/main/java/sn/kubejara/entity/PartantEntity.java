package sn.kubejara.entity;

import jakarta.persistence.*;
import lombok.*;

@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Table(name = "partants")
@Entity
public class PartantEntity {

    @Id
    private  Long numero;
    private  String nom;
    @ManyToOne(cascade = CascadeType.ALL)
    private CourseEntity course;
}
