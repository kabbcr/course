package sn.kubejara.mapper;

import org.springframework.stereotype.Component;
import sn.kubejara.entity.Course;
import sn.kubejara.entity.CourseEntity;
import sn.kubejara.entity.Partant;
import sn.kubejara.entity.PartantEntity;

import java.util.Set;
import java.util.stream.Collectors;

@Component
public class CourseDataAccessMapper {

    public CourseEntity courseToCourseEntity(Course course){
        CourseEntity courseEntity = CourseEntity.builder()
                .numero(course.getNumero())
                .nom(course.getNom())
                .date(course.getDate())
                .partants(partantsToPartantEntities(course.getPartants()))
                .build();
        courseEntity.getPartants().forEach(partantEntity -> partantEntity.setCourse(courseEntity));
        return courseEntity;
    }


    private Set<PartantEntity> partantsToPartantEntities(Set<Partant> partants){
        return partants.stream()
                .map(partant -> PartantEntity.builder()
                        .numero((long) partant.numero())
                        .nom(partant.nom())
                        .build())
                .collect(Collectors.toSet());
    }

    public Course courseEntityToCourse(CourseEntity courseEntity){
        return Course.Builder.builder()
                .numero(courseEntity.getNumero())
                .nom(courseEntity.getNom())
                .date(courseEntity.getDate())
                .partants(partantEntitiesToPartant(courseEntity.getPartants()))
                .build();
    }

    private Set<Partant> partantEntitiesToPartant(Set<PartantEntity> partantEntities){
        return partantEntities.stream()
                .map(partantEntity -> new Partant(partantEntity.getNumero().intValue(), partantEntity.getNom()))
                .collect(Collectors.toSet());
    }
}
