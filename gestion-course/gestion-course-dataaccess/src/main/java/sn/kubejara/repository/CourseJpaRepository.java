package sn.kubejara.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import sn.kubejara.entity.CourseEntity;

@Repository
public interface CourseJpaRepository extends JpaRepository<CourseEntity,Long> {
}
