package sn.kubejara.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import sn.kubejara.entity.PartantEntity;

public interface PartantJpaRepository extends JpaRepository<PartantEntity,Long> {
}
