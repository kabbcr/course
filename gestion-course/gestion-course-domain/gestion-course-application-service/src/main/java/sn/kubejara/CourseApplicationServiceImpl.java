package sn.kubejara;

import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.validation.annotation.Validated;
import sn.kubejara.dto.create.CourseDto;
import sn.kubejara.event.CourseCreatedEvent;
import sn.kubejara.ports.input.service.CourseApplicationService;

@Slf4j
@Validated
@Service
public class CourseApplicationServiceImpl implements CourseApplicationService {

    private final CourseHandler courseHandler;

    public CourseApplicationServiceImpl(CourseHandler courseHandler) {
        this.courseHandler = courseHandler;
    }


    @Override
    public CourseCreatedEvent creerCourse(CourseDto courseDto) {
      return courseHandler.creerCourse(courseDto);
    }
}
