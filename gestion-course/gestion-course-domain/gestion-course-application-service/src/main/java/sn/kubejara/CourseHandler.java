package sn.kubejara;

import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;
import sn.kubejara.dto.create.CourseDto;
import sn.kubejara.entity.Course;
import sn.kubejara.event.CourseCreatedEvent;
import sn.kubejara.exception.CourseDomainException;
import sn.kubejara.mapper.CourseDataMapper;
import sn.kubejara.ports.output.repository.CourseRepository;

@Component
@Slf4j
public class CourseHandler {

    private final CourseDomainService courseDomainService;
    private final CourseRepository courseRepository;


    private final CourseDataMapper courseDataMapper;

    public CourseHandler(CourseDomainService courseDomainService,
                         CourseRepository courseRepository,
                         CourseDataMapper courseDataMapper) {
        this.courseDomainService = courseDomainService;
        this.courseRepository = courseRepository;
        this.courseDataMapper = courseDataMapper;
    }


    public CourseCreatedEvent creerCourse(CourseDto courseDto) {
        Course course = courseDataMapper.courseDtoToCourse(courseDto);
        CourseCreatedEvent courseCreatedEvent = courseDomainService.creerCourse(course);
        save(course);
        return  courseCreatedEvent;
    }

    private Course save(Course course) {
        Course courseResult = courseRepository.save(course);
        if (courseResult == null) {
            log.error("Impossible d'enregistrer la course!");
            throw new CourseDomainException("Impossible d'enregistrer la course!");
        }
        log.info("La course numéro: {} est bien enregistrée.", courseResult.getNumero());
        return courseResult;
    }
}
