package sn.kubejara.dto.create;

import jakarta.validation.constraints.NotNull;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import sn.kubejara.entity.Partant;

import java.time.ZonedDateTime;
import java.util.Set;

@Getter
@Builder
@AllArgsConstructor
public class CourseDto {

    @NotNull
    private final Long numero;
    @NotNull
    private final String nom;
    @NotNull
    private final ZonedDateTime date;
    @NotNull
    private final Set<Partant> partants;
}
