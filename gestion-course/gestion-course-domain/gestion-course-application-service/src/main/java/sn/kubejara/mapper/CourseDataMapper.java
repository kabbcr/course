package sn.kubejara.mapper;

import org.springframework.stereotype.Component;
import sn.kubejara.dto.create.CourseDto;
import sn.kubejara.entity.Course;

@Component
public class CourseDataMapper {

    public Course courseDtoToCourse(CourseDto courseDto){
        return Course.Builder.builder()
                .numero(courseDto.getNumero())
                .nom(courseDto.getNom())
                .date(courseDto.getDate())
                .partants(courseDto.getPartants())
                .build();
    }

}
