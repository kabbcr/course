package sn.kubejara.ports.input.service;

import jakarta.validation.Valid;
import sn.kubejara.dto.create.CourseDto;
import sn.kubejara.event.CourseCreatedEvent;

public interface CourseApplicationService {

    CourseCreatedEvent creerCourse(@Valid CourseDto createCourseCommand);
}
