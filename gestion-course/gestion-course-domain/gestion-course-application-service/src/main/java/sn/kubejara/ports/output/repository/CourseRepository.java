package sn.kubejara.ports.output.repository;

import sn.kubejara.entity.Course;

public interface CourseRepository {


    Course save(Course course);

}
