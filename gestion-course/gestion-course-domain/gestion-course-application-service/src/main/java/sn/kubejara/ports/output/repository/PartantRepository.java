package sn.kubejara.ports.output.repository;

import sn.kubejara.entity.Partant;

public interface PartantRepository {

    Partant save(Partant partant);

}
