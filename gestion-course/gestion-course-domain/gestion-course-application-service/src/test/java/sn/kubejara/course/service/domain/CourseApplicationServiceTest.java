package sn.kubejara.course.service.domain;


import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import sn.kubejara.dto.create.CourseDto;
import sn.kubejara.entity.Course;
import sn.kubejara.entity.Partant;
import sn.kubejara.event.CourseCreatedEvent;
import sn.kubejara.mapper.CourseDataMapper;
import sn.kubejara.ports.input.service.CourseApplicationService;
import sn.kubejara.ports.output.repository.CourseRepository;

import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.util.Set;

@TestInstance(TestInstance.Lifecycle.PER_CLASS)
@SpringBootTest(classes = CourseTestConfiguration.class)
public class CourseApplicationServiceTest {


    @Autowired
    private CourseApplicationService courseApplicationService;
    @Autowired
    private CourseDataMapper courseDataMapper;
    @Autowired
    private CourseRepository courseRepository;


    private CourseDto courseAvecNombrePartantsCorrect;

    @BeforeAll
    public void init(){
        courseAvecNombrePartantsCorrect = CourseDto.builder()
                .numero(1L)
                .nom("Course de France")
                .date(ZonedDateTime.of(2023,12,24, 10,30, 0, 0, ZoneId.of("UTC")))
                .partants(Set.of(new Partant(1,"A"),new Partant(2,"B"), new Partant(3,"C")))
                .build();
        Course course = courseDataMapper.courseDtoToCourse(courseAvecNombrePartantsCorrect);
        Mockito.when(courseRepository.save(Mockito.any(Course.class))).thenReturn(course);
    }

    @Test
    public void creerCourse(){
        CourseCreatedEvent courseCreatedEvent = courseApplicationService.creerCourse(courseAvecNombrePartantsCorrect);
        Assertions.assertEquals(courseCreatedEvent.getCourse().getNumero(), courseAvecNombrePartantsCorrect.getNumero());
    }

}
