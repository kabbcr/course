package sn.kubejara.course.service.domain;


import org.mockito.Mockito;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import sn.kubejara.CourseDomainService;
import sn.kubejara.CourseDomainServiceImpl;
import sn.kubejara.ports.output.repository.CourseRepository;
import sn.kubejara.ports.output.repository.PartantRepository;

@SpringBootApplication(scanBasePackages = "sn.kubejara")
public class CourseTestConfiguration {

    @Bean
    public CourseRepository courseRepository(){
        return Mockito.mock(CourseRepository.class);
    }

    @Bean
    public PartantRepository partantRepository(){
        return Mockito.mock(PartantRepository.class);
    }

    @Bean
    public CourseDomainService accountDomainService(){
        return new CourseDomainServiceImpl();
    }
}
