package sn.kubejara;

import sn.kubejara.entity.Course;
import sn.kubejara.event.CourseCreatedEvent;

public interface CourseDomainService {

    CourseCreatedEvent creerCourse(Course course);
}
