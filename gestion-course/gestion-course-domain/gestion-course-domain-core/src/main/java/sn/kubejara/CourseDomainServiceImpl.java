package sn.kubejara;

import lombok.extern.slf4j.Slf4j;
import sn.kubejara.entity.Course;
import sn.kubejara.event.CourseCreatedEvent;

import java.time.ZoneId;
import java.time.ZonedDateTime;

import static sn.kubejara.domain.constant.DomainConstants.UTC;

@Slf4j
public class CourseDomainServiceImpl implements CourseDomainService{

    @Override
    public CourseCreatedEvent creerCourse(Course course) {
        course.initialiserCourse(course);
        log.info("La course avec l'identifiant : {} est créée.", course.getNumero());
        return new CourseCreatedEvent(course, ZonedDateTime.now(ZoneId.of(UTC)));
    }
}
