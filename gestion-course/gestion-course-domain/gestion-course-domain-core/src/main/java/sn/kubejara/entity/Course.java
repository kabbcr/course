package sn.kubejara.entity;

import sn.kubejara.exception.CourseDomainException;

import java.time.ZonedDateTime;
import java.util.Set;

import static sn.kubejara.domain.constant.DomainConstants.MIN_PARTANTS;
import static sn.kubejara.domain.constant.DomainConstants.PREMIER_NUMERO;
import static sn.kubejara.domain.constant.DomainMessages.*;


public class Course {
    private final Long numero;
    private final String nom;
    private final ZonedDateTime date;
    private final Set<Partant> partants;


    public void initialiserCourse(Course course){
        validerCourse(course.getDate(),course.getPartants());
        Builder.builder()
                .numero(course.getNumero())
                .nom(course.getNom())
                .date(course.getDate())
                .partants(course.getPartants())
                .build();
    }

    private void validerCourse(ZonedDateTime date, Set<Partant> partants){
        validerDateCourse(date);
        validerPartants(partants);
    }

    private void validerDateCourse(ZonedDateTime date){
        if(date.isBefore(ZonedDateTime.now())){
            throw new CourseDomainException(DATE_COURSE_INCORRECTE_MESSAGE);
        }
    }

    private void validerPartants(Set<Partant> partants){
        if(partants.size() < MIN_PARTANTS){
            throw new CourseDomainException(NOMBRE_PARTANTS_INCORRECT_MESSAGE);
        }
        for(Partant partant : partants){
            if(partant.numero() < PREMIER_NUMERO || partant.numero() > partants.size()){
                throw new CourseDomainException(NUMEROTATION_INCORRECTE_MESSAGE);
            }
        }

    }

    public Long getNumero() {
        return numero;
    }

    public String getNom() {
        return nom;
    }

    public ZonedDateTime getDate() {
        return date;
    }

    public Set<Partant> getPartants() {
        return partants;
    }

    private Course(Builder builder) {
        numero = builder.numero;
        nom = builder.nom;
        date = builder.date;
        partants = builder.partants;
    }


    public static final class Builder {
        private Long numero;
        private String nom;
        private ZonedDateTime date;
        private Set<Partant> partants;

        private Builder() {
        }

        public static Builder builder() {
            return new Builder();
        }

        public Builder numero(Long val) {
            numero = val;
            return this;
        }

        public Builder nom(String val) {
            nom = val;
            return this;
        }

        public Builder date(ZonedDateTime val) {
            date = val;
            return this;
        }

        public Builder partants(Set<Partant> val) {
            partants = val;
            return this;
        }

        public Course build() {
            return new Course(this);
        }
    }
}
