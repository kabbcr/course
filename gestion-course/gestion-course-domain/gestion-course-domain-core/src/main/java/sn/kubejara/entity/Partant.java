package sn.kubejara.entity;

import java.util.Objects;

public record Partant(int numero, String nom)  {
}
