package sn.kubejara.event;


import sn.kubejara.entity.Course;

import java.time.ZonedDateTime;

public class CourseCreatedEvent extends CourseEvent {
    public CourseCreatedEvent(Course course, ZonedDateTime createdAt) {
        super(course, createdAt);
    }
}
