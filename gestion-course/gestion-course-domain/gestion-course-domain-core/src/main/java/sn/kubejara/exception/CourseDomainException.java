package sn.kubejara.exception;

import sn.kubejara.domain.exception.DomainException;

public class CourseDomainException extends DomainException {
    public CourseDomainException(String message) {
        super(message);
    }

    public CourseDomainException(String message, Throwable cause) {
        super(message, cause);
    }
}
